module gitlab.com/metakeule/goh4

go 1.23.0

require (
	gitlab.com/golang-utils/updatechecker v0.0.6
	gitlab.com/metakeule/places v1.5.0
	gitlab.com/metakeule/templ v0.4.0
)
