package goh4

import (
	"fmt"
	"html"
	"regexp"
	"strings"

	"gitlab.com/golang-utils/updatechecker"
)

func init() {
	updatechecker.PrintVersionCheck("gitlab.com/metakeule/goh4")
}

// type Context string

// func (xyyy Context) String() string { return string(xyyy) }

type Comment string

func (xyyy Comment) String() string { return string(xyyy) }

type Class string

func (xyyy Class) String() string { return string(xyyy) }

func (xyyy Class) Selector() string { return fmt.Sprintf(".%s", xyyy) }

func Classf(format string, i ...interface{}) Class {
	return Class(fmt.Sprintf(format, i...))
}

type classes []Class

func Classes(c ...string) classes {
	cl := classes{}
	for _, cc := range c {
		cl = append(cl, Class(cc))
	}
	return cl
}

/*
func (xyyy Class) Rule() (r *RuleStruct) {
    return &RuleStruct{Selector: xyyy}
}
*/

type Id string

func (xyyy Id) String() string { return string(xyyy) }

func (xyyy Id) Selector() string { return fmt.Sprintf("#%s", xyyy) }

/*
func (xyyy Id) Rule() (r *RuleStruct) {
    return &RuleStruct{Selector: xyyy}
}
*/

type Text string

func (xyyy Text) String() string { return string(xyyy) }

func Textf(format string, i ...interface{}) Text {
	return Text(fmt.Sprintf(format, i...))
}

type Html string

func (xyyy Html) String() string { return string(xyyy) }

func Htmlf(format string, i ...interface{}) Html {
	return Html(fmt.Sprintf(format, i...))
}

type Tag string

func (xyyy Tag) String() string { return string(xyyy) }

func (xyyy Tag) Selector() string { return xyyy.String() }

type SingleAttr struct {
	Key   string
	Value string
}

func (xyyy SingleAttr) String() string {
	return " " + xyyy.Key + `="` + html.EscapeString(xyyy.Value) + `"`
}

type Attrs []SingleAttr

func (xyyy Attrs) String() (s string) {
	ss := []string{}
	for _, atr := range xyyy {
		ss = append(ss, atr.String())
	}
	return strings.Join(ss, " ")
}

type tags []Tag

func Tags(tag string, xyyy ...string) (a tags) {
	a = tags{Tag(tag)}
	for _, s := range xyyy {
		a = append(a, Tag(s))
	}
	return
}

func (xyyy tags) String() string {
	str := []string{}
	for _, t := range xyyy {
		str = append(str, t.String())
	}
	return strings.Join(str, ", ")
}

// helper to easily create multiple SingleAttrs
// use is like this
// Attr("width","200px","height","30px","value","hiho")
func Attr(key1, val1 string, xyyy ...string) (s Attrs) {
	s = []SingleAttr{{key1, val1}}
	for i := 0; i < len(xyyy); i = i + 2 {
		s = append(s, SingleAttr{xyyy[i], xyyy[i+1]})
	}
	return
}

/*
type Placeholder string

//func (xyyy Placeholder) String() string { return string(places.DefaultStartDel) + string(xyyy) + string(places.DefaultEndDel) }
func (xyyy Placeholder) Name() string   { return string(xyyy) }
func (xyyy Placeholder) String() string { return string(xyyy) }

func (xyyy Placeholder) Key() string { return string(places.DefaultStartDel) + string(xyyy) + string(places.DefaultEndDel) }

//func (xyyy Placeholder) Html() Html     { return Html(xyyy.String()) }
*/

type Stringer interface {
	String() string
}

type Selecter interface {
	Selector() string
}

func Context(ctx Selecter, inner1 Selecter, inner ...Selecter) (r Selecter) {
	sel := []string{}
	inner = append(inner, inner1)
	for _, i := range inner {
		sel = append(sel, ctx.Selector()+" "+i.Selector())
	}
	return SelectorString(strings.Join(sel, ",\n"))
}

func ContextString(ctx string, inner1 Selecter, inner ...Selecter) (r Selecter) {
	return Context(SelectorString(ctx), inner1, inner...)
}

type SelecterAdder interface {
	Selector() string
	Add(Selecter) SelecterAdder
}

type SelectorString string

func (xyyy SelectorString) Selector() string { return string(xyyy) }

// combine several selectors to one
func Selector(sel1 Selecter, selects ...Selecter) Selecter {
	s := []string{sel1.Selector()}
	for _, sel := range selects {
		s = append(s, sel.Selector())
	}
	return SelectorString(strings.Join(s, ""))
}

/*
func (xyyy Tag) Rule() (r *RuleStruct) {
	return &RuleStruct{Selector: xyyy}
}
*/

type Scss string

func (xyyy Scss) String() string { return string(xyyy) }

func removeWhiteSpace(in string) string {
	reg := regexp.MustCompile(`\s`)
	return reg.ReplaceAllString(in, "")
}

/*
type style struct {
	Key   string
	Value string
}

func (xyyy style) String() string { return xyyy.Key + ": " + xyyy.Value + ";" }

type styles []style

func (xyyy styles) String() (s string) {
	ss := []string{}
	for _, st := range xyyy {
		ss = append(ss, st.String())
	}
	return strings.Join(ss, " ")
}

// helper to easily create multiple styles
// use is like this
// Style("width","200px","height","30px","color","green")
func Styles(xyyy ...string) (s styles) {
	s = styles{}
	for i := 0; i < len(xyyy); i = i + 2 {
		s = append(s, style{xyyy[i], xyyy[i+1]})
	}
	return
}
*/
