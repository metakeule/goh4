package goh4

type Style struct{ Property, Value string }

func (xyyy Style) Val(v string) Style { xyyy.Value = v; return xyyy }
func (xyyy Style) String() string     { return xyyy.Property + ": " + xyyy.Value + ";" }
func (xyyy Style) Style() string      { return xyyy.String() }

func Styles(styles ...Style) (s []Style) {
	return styles
}
