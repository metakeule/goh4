package main

import (
	"fmt"

	. "gitlab.com/metakeule/goh4/css"
	"gitlab.com/metakeule/goh4/styl"
	. "gitlab.com/metakeule/goh4/styl/color"
	. "gitlab.com/metakeule/goh4/tag"
)

var bestClass = CLASS("best")
var mainId = ID("main")
var newsClass = CLASS("news")

func main() {
	css := Css(DIV(mainId, bestClass),
		styl.BackgroundColor(GREEN),

		Css(SECTION(newsClass),
			styl.BorderBottom(styl.Px(2), GREEN, styl.Dotted_),

			Css(ARTICLE(),
				styl.FontSize(styl.Px(12)),

				Css(H2(),
					styl.FontSize(styl.Px(25)),

					Css(P(),
						styl.MarginBottom(styl.Px(10)),
					),
				),
			),
		),
	)

	fmt.Println(css)
}
