package css

import (
	"fmt"
	"strings"

	. "gitlab.com/metakeule/goh4"
)

type Styler interface {
	Style() string
}

var ParentSelector = SelectorString("&")

func Super(sel ...Selecter) Selecter {
	return Selector(ParentSelector, sel...)
}

type CssStringer interface {
	Css() string
}

type Import string

func (xyyy Import) String() string {
	return fmt.Sprintf("@import %#v;\n", string(xyyy))
}

type nest RuleStruct

func Nest(xs ...interface{}) (r *nest) {
	rl, err := Rule(xs...)
	if err != nil {
		panic(err.Error())
	}
	n := nest(*rl)
	r = &n
	return
}

type RuleStruct struct {
	Comment     string
	Selector    Selecter
	Styles      []Styler
	nested      []*RuleStruct
	children    []*RuleStruct
	cssStringer []CssStringer
	Parent      *RuleStruct
}

func Rule(xs ...interface{}) (r *RuleStruct, err error) {
	r = &RuleStruct{}
	r.Styles = []Styler{}
	r.nested = []*RuleStruct{}
	r.children = []*RuleStruct{}
	r.cssStringer = []CssStringer{}
	// r.Selector = SelectorString("")
	for _, x := range xs {
		switch v := x.(type) {
		// we don't want to handle *RuleStruct and *rules here,
		// since there would be different ways to handle them (Next, Embed, Compose )
		// and we don't want to have a implicit default way
		case *nest:
			rls := RuleStruct(*v)
			r.nested = append(r.nested, &rls)
		case *RuleStruct:
			r.children = append(r.children, v)
		case CssStringer:
			r.cssStringer = append(r.cssStringer, v)
		case Selecter:
			r.Selector = v
		case Styler:
			r.Styles = append(r.Styles, v)
		case []Style:
			r.Style(v...)
		case []Styler:
			r.Styles = append(r.Styles, v...)
		case string:
			r.Comment = v
		case Comment:
			r.Comment = string(v)
		default:
			err = fmt.Errorf("%T is not an allowed type", x)
			return
		}
	}
	return
}

// for each selector, my selectors is prefixed and
// my rules are applied
func (xyyy *RuleStruct) ForEach(c SelecterAdder, sel ...Selecter) (*RuleStruct, error) {
	comb := c.Add(xyyy.Selector)
	all := Each()
	for _, s := range sel {
		all.Add(comb.Add(s))
	}
	return Rule(all, xyyy.Styles)
}

func (xyyy *RuleStruct) String() string {
	styles := []string{}
	// fmt.Println(xyyy)
	for _, st := range xyyy.Styles {
		styles = append(styles, st.Style())
	}
	comment := ""
	if xyyy.Comment != "" {
		comment = fmt.Sprintf("/* %s */\n", xyyy.Comment)
	}
	var my string
	if xyyy.Selector != nil {
		strs := []string{}
		strs = append(strs, xyyy.Selector.Selector())
		nested := []string{}
		for _, nr := range xyyy.nested {
			ns := nr.String()
			nssp := strings.Split(ns, "\n")
			for _, nsi := range nssp {
				nested = append(nested, nsi)
			}
		}
		my = fmt.Sprintf("%s%s {\n\t%s\n\t%s\n}", comment, strings.Join(strs, ",\n"), strings.Join(styles, "\n\t"), strings.Join(nested, "\n\t"))
	} else {
		my = ""
	}
	all := []string{my}

	for _, cs := range xyyy.cssStringer {
		all = append(all, cs.Css())
	}

	for _, child := range xyyy.children {
		if xyyy.Selector != nil {
			nu := child.Embed(xyyy.Selector)
			all = append(all, nu.String())
		} else {
			all = append(all, child.String())
		}
	}
	return strings.Join(all, "\n")
}

// adds given styles
func (xyyy *RuleStruct) Style(styles ...Style) *RuleStruct {
	for _, st := range styles {
		xyyy.Styles = append(xyyy.Styles, st)
	}
	return xyyy
}

func (xyyy *RuleStruct) Nest(xs ...interface{}) (i *RuleStruct, err error) {
	i, err = Rule(xs...)
	if err != nil {
		return
	}
	xyyy.nested = append(xyyy.nested, i)
	return
}

func (xyyy *RuleStruct) _inner(sa SelecterAdder, sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	i, err = Rule(xs...)
	if err != nil {
		return
	}
	i.Selector = sa.Add(xyyy.Selector).Add(sel)
	return
}

func (xyyy *RuleStruct) Descendant(sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	return xyyy._inner(Descendant(), sel, xs...)
}

func (xyyy *RuleStruct) Child(sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	return xyyy._inner(Child(), sel, xs...)
}

func (xyyy *RuleStruct) DirectFollows(sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	return xyyy._inner(DirectFollows(), sel, xs...)
}

func (xyyy *RuleStruct) Follows(sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	return xyyy._inner(Follows(), sel, xs...)
}

func (xyyy *RuleStruct) Each(sel Selecter, xs ...interface{}) (i *RuleStruct, err error) {
	return xyyy._inner(Each(), sel, xs...)
}

// returns a copy
func (xyyy *RuleStruct) Copy() (newrule *RuleStruct) {
	newStyles := []Styler{}
	for _, st := range xyyy.Styles {
		newStyles = append(newStyles, st)
	}

	newnested := []*RuleStruct{}
	for _, st := range xyyy.nested {
		newnested = append(newnested, st)
	}

	newchildren := []*RuleStruct{}
	for _, st := range xyyy.children {
		newchildren = append(newchildren, st)
	}

	newrule = &RuleStruct{
		Comment:  xyyy.Comment,
		Styles:   newStyles,
		Selector: xyyy.Selector,
		nested:   newnested,
		children: newchildren,
		Parent:   xyyy.Parent,
	}
	return
}

// returns a copy that is embedded in the selector
func (xyyy *RuleStruct) Embed(selector Selecter) (newrule *RuleStruct) {
	newrule = xyyy.Copy()
	newSelector := SelectorString(selector.Selector() + " " + xyyy.Selector.Selector())
	newrule.Selector = newSelector
	return
}

// returns a copy that is a composition of this rule with the styles
// of other rules
func (xyyy *RuleStruct) Compose(parents ...*RuleStruct) (newrule *RuleStruct) {
	newrule = xyyy.Copy()
	for _, parent := range parents {
		for _, st := range parent.Styles {
			newrule.Styles = append(newrule.Styles, st)
		}
	}
	return
}

type rules struct {
	rules []*RuleStruct
}

func Rules() *rules {
	r := []*RuleStruct{}
	return &rules{r}
}

func (xyyy *rules) Add(r *RuleStruct) {
	xyyy.rules = append(xyyy.rules, r)
}

func (xyyy *rules) New(xs ...interface{}) (r *RuleStruct, err error) {
	r, err = Rule(xs...)
	if err != nil {
		return
	}
	xyyy.Add(r)
	return
}

func (xyyy *rules) String() string {
	rules := []string{}

	for _, r := range xyyy.rules {
		rules = append(rules, r.String())
	}

	return strings.Join(rules, "\n\n")
}

// returns a copy with all rules embedded with selector
func (xyyy *rules) Embed(selector Selecter) *rules {
	newRules := []*RuleStruct{}
	for _, r := range xyyy.rules {
		newRules = append(newRules, r.Embed(selector))
	}
	return &rules{newRules}
}

/*
type Css []Stringer

func (xyyy Css) String() string {
	rules := []string{}

	for _, r := range xyyy {
		rules = append(rules, r.String())
	}

	return strings.Join(rules, "\n\n")
}
*/

func Css(xs ...interface{}) *RuleStruct {
	r, ſ := Rule(xs...)
	if ſ != nil {
		panic(ſ.Error())
	}
	return r
}
