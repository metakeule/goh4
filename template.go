package goh4

import (
	"fmt"
)

type Templatable interface {
	String() string
	Tag() string
	Path() string
	Any(m Matcher) (r *Element)
	Add(objects ...interface{}) (err error)
}

type Template struct {
	Element          Templatable //*Element
	locals           map[Id]Stringer
	placeholderCache map[Id]*Element
}

// creates a new template with the given element as root
func NewTemplate(t Templatable) *Template {
	return &Template{
		Element:          t,
		locals:           map[Id]Stringer{},
		placeholderCache: map[Id]*Element{},
	}
}

func (xyyy *Template) String() string {
	return xyyy.Element.String()
}

func (xyyy *Template) Add(objects ...interface{}) (err error) {
	return xyyy.Element.Add(objects...)
}

// merges the locals to the Templates
func (xyyy *Template) merge() {
	for k, v := range xyyy.locals {
		oldParent := xyyy.placeholderCache[k].Parent()

		elem, isElement := v.(*Element)
		if isElement {
			elem.SetParent(oldParent)
			xyyy.placeholderCache[k].SetContent(elem)
		} else {
			xyyy.placeholderCache[k].SetContent(v)
		}

	}
}

// caches the Stringer
func (xyyy *Template) cacheFragment(id Id) (err error) {
	h := xyyy.Element.Any(Id(id))
	if h == nil {
		return fmt.Errorf("element with id %v not found in %s", id, xyyy.Element.String())
	}
	xyyy.placeholderCache[id] = h
	return
}

// replaces the content of an child Element with the given id with Stringer e.g.
//
//	t := NewTemplate(Body(Div(Id("content"))))
//	t.Assign("content", P(Text("here we go")))
//
// results in <body><div id="content"><p>here we go</p></div></body>
func (xyyy *Template) Assign(id Id, html interface{}) (err error) {
	if xyyy.placeholderCache[id] == nil {
		if err = xyyy.cacheFragment(id); err != nil {
			return err
		}
	}
	switch v := html.(type) {
	case string:
		xyyy.locals[id] = Text(v)
	default:
		xyyy.locals[id] = v.(Stringer)
	}

	xyyy.merge()
	return
}

// add css to the head of the template
// returns an error if Element is no doc or has no head child
func (xyyy *Template) AddCss(css ...Stringer) (err error) {
	if xyyy.Element.Tag() != "doc" {
		return fmt.Errorf("can't add Css only to doc pseudotag, not %s", xyyy.Element.Tag())
	}

	style := xyyy.Element.Any(Tag("style"))
	if style == nil {
		head := xyyy.Element.Any(Tag("head"))
		if head == nil {
			return fmt.Errorf("no head element present in %s", xyyy.Element.Path())
		}
		style = NewElement(Tag("style"), Invisible, WithoutEscaping)
		head.Add(style)
	}

	for _, cs := range css {
		style.Add(Html(cs.String()))
	}
	return
}
