package tag

import (
	. "gitlab.com/metakeule/goh4"
)

func CLASS(c string) Class                             { return Class(c) }
func ID(id string) Id                                  { return Id(id) }
func HTML(html string) Html                            { return Html(html) }
func TAG(s string) Tag                                 { return Tag(s) }
func ATTR(key1, val1 string, xyyy ...string) (s Attrs) { return Attr(key1, val1, xyyy...) }
