package goh4

import (
	"bytes"
	"fmt"
	"html"
	"io"
	"net/http"
	"strings"

	"gitlab.com/metakeule/places"
	"gitlab.com/metakeule/templ"
)

type flag int

const (
	_                      = iota
	hasDefaults       flag = 1 << iota // element has default flags
	IdForbidden                        // element should not have an id attribute
	ClassForbidden                     // element should not have a class attribute
	SelfClosing                        // element is selfclosing and contains no content
	Inline                             // element is an inline element (only for visible elements)
	FormField                          // element is a field of a form
	Invisible                          // element doesn't render anything visible
	WithoutEscaping                    // element does not escape inner Text
	WithoutDecoration                  // element just prints the InnerHtml
)

var flagNames = map[flag]string{
	hasDefaults:       "hasDefaults",
	IdForbidden:       "IdForbidden",
	ClassForbidden:    "ClassForbidden",
	SelfClosing:       "SelfClosing",
	Inline:            "Inline",
	FormField:         "FormField",
	Invisible:         "Invisible",
	WithoutEscaping:   "WithoutEscaping",
	WithoutDecoration: "WithoutDecoration",
}

func (xyyy flag) String() string {
	return flagNames[xyyy]
}

type Pather interface {
	Path() string
}
type Tager interface {
	Tag() string
}

// a Csser might be applied as Css to an Element
type Csser interface {
	Matcher
	Class() string
}

// an Elementer might be parent of an Element
// by implementing a type that fulfills this interface
// you might peek into the execution.
// when String() method is called, the html of the
// tree is built and when SetParent() it is embedded in another Elementer
// it could be combined with the Pather interface that allows you to modify specific
// css selectors for any children Elements
type Elementer interface {
	Stringer
	Tager
	IsParentAllowed(Tager) bool
	SetParent(Pather)
}

// the base of what becomes a tag when printed
type Element struct {
	attributes map[string]string
	Comment    Comment
	parent     Pather
	classes    []Class
	id         Id
	inner      []Stringer
	ParentTags tags
	style      map[string]string
	flags      flag
	tag        Tag
}

// contruct a new element with some flags.
//
// the tag constructors A(), Body(),... use these method, see tags.go file for examples
//
// use it for your own tags
//
// the following flags are supported
//
//	IdForbidden                        // element should not have an id attribute
//	ClassForbidden                     // element should not have a class attribute
//	SelfClosing                        // element is selfclosing and contains no content
//	Inline                             // element is an inline element (only for visible elements)
//	Field                              // element is a field of a form
//	Invisible                          // element doesn't render anything visible
//	WithoutEscaping                    // element does not escape inner Text
//	WithoutDecoration                  // element just prints the InnerHtml
//
// see Add() and Set() methods for how to modify the Element
func NewElement(t Tag, flags ...flag) (xyyy *Element) {
	xyyy = &Element{
		attributes: map[string]string{},
		tag:        t,
		flags:      hasDefaults,
		style:      map[string]string{},
		ParentTags: tags{},
	}

	for _, flag := range flags {
		xyyy.flags = xyyy.flags | flag
	}
	return
}

func (xyyy *Element) NotEscape() *Element {
	xyyy.Add(WithoutEscaping)
	return xyyy
}

// checks if a given flag is set, e.g.
//
//	Is(Inline)
//
// checks for the Inline flag
func (xyyy *Element) Is(f flag) bool {
	return xyyy.flags&f != 0
}

// returns the formfields
func (xyyy *Element) Fields() (fields []*Element) {
	return xyyy.All(FieldMatcher(0))
}

func (xyyy *Element) Parent() Pather {
	return xyyy.parent
}

func (xyyy *Element) SetAttributes(a ...string) {
	if len(a)%2 != 0 {
		panic("no even number, need pairs of key, val")
	}
	for i := 0; i < len(a); i += 2 {
		xyyy.SetAttribute(a[i], a[i+1])
	}
}

// sets the attribute k to v as long as k is not "id" or "class"
// use SetId() to set the id and AddClass() to add a class
func (xyyy *Element) SetAttribute(k, v string) {
	if k != "id" && k != "class" {
		xyyy.attributes[k] = v
	}
}

// removes an attribute
func (xyyy *Element) RemoveAttribute(k string) {
	delete(xyyy.attributes, k)
}

func (xyyy *Element) Attribute(k string) string {
	return xyyy.attributes[k]
}

func (xyyy *Element) Attributes() map[string]string {
	return xyyy.attributes
}

// wraps the children with the given element
func (xyyy *Element) WrapChildren(wrapper *Element) {
	for _, s := range xyyy.inner {
		wrapper.Add(s)
	}
	xyyy.inner = []Stringer{}
	xyyy.Add(wrapper)
}

func (xyyy *Element) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(xyyy.String()))
}

func (xyyy *Element) WriteTo(w io.Writer) {
	w.Write([]byte(xyyy.String()))
}

func (xyyy *Element) SetParent(parent Pather) {
	xyyy.parent = parent
}

func (xyyy *Element) Tag() string {
	return xyyy.tag.String()
}

func (xyyy *Element) Path() string {
	parentPath := ""
	if xyyy.parent != nil {
		parentPath = xyyy.parent.Path() + " "
	}
	return fmt.Sprintf(parentPath+"%s%s%s", xyyy.tag, xyyy.idPath(), xyyy.classPath(xyyy.classes))
}

// creates a Css with Context set to Path()
/*
func (xyyy *Element) NewCss(objects ...Stringer) *Css {
	objects = append(objects, Context(xyyy.Path()))
	return NewCss(objects...)
}
*/

func (xyyy *Element) idPath() (s string) {
	s = ""
	if !xyyy.Is(IdForbidden) && xyyy.id != "" {
		s = "#" + xyyy.id.String()
	}
	return
}

// return false if the given Parent tag is not allowed for Elements tag
func (xyyy *Element) IsParentAllowed(parent Tager) (allowed bool) {
	if len(xyyy.ParentTags) == 0 {
		return true
	}
	allowed = false
	for _, p := range xyyy.ParentTags {
		if p.String() == parent.Tag() {
			allowed = true
		}
	}
	return
}

// adds css properties to the style attribute, same keys are overwritten
func (xyyy *Element) addStyle(styls ...interface{}) {
	if xyyy.Is(Invisible) {
		// can't set style for invisible tag
		return
	}
	for _, s := range styls {
		switch v := s.(type) {
		case []Style:
			for _, st := range v {
				xyyy.style[st.Property] = st.Value
			}
		case Style:
			xyyy.style[v.Property] = v.Value
			/*
				case styles:
					for _, styl := range v {
						xyyy.style[styl.Key] = styl.Value
					}
			*/
		case string:
			a := strings.Split(v, ":")
			xyyy.style[a[0]] = strings.Replace(a[1], ";", "", 1)
		}
	}
}

// apply the css to the element, i.e. add the class of the Css to the Element
//
// returns an error if the Css has no class or if the Css will not match
// the Element because of its tags or its context
// beware that it will always fail if the Css has a context, because the
// matcher can't properly figure out if an element is within a context
// that remains to be done (but requires a proper css selector parser)
/*
func (xyyy *Element) ApplyCss(rules ...Csser) (err error) {
	for _, r := range rules {
		if r.Class() == "" {
			return fmt.Errorf("can't apply css rule without a class: %s", r)
		}

		if err := xyyy.AddClass(Class(r.Class())); err != nil {
			return err
		}

		if !r.Matches(xyyy) {
			return fmt.Errorf("tag %s not affected by css rule %#v", xyyy.Path(), r)
		}
	}
	return
}
*/

// returns an error if the Element is self closing
func (xyyy *Element) ensureContentAddIsAllowed() (err error) {
	if xyyy.Is(SelfClosing) {
		return fmt.Errorf("add not allowed for tag »%s«", xyyy.tag)
	}
	return
}

// adds Elementer to the inner content at position pos
func (xyyy *Element) AddAtPosition(pos int, v Elementer) (err error) {
	if pos < 0 || pos > len(xyyy.inner)-1 {
		return fmt.Errorf("position %v out of range", pos)
	}
	if err := xyyy.ensureParentIsSetAndContentIsAllowed(v); err != nil {
		return err
	}
	before := xyyy.inner[0:pos]
	after := xyyy.inner[pos:len(xyyy.inner)]

	newSlice := make([]Stringer, (1 + len(xyyy.inner)))
	newSlice[pos] = v
	for i := 0; i < len(before); i++ {
		newSlice[i] = before[i]
	}

	for i := 0; i < len(after); i++ {
		newSlice[len(before)+i+1] = after[i]
	}
	xyyy.inner = newSlice
	return
}

// make sure the parent is set and content is allowed
func (xyyy *Element) ensureParentIsSetAndContentIsAllowed(v Stringer) error {
	e, isElementer := v.(Elementer)
	if isElementer {
		if !e.IsParentAllowed(xyyy) {
			return fmt.Errorf("tag %s not allowed as parent of %s", xyyy.Tag(), e.Tag())
		}
		e.SetParent(xyyy)
	}
	return xyyy.ensureContentAddIsAllowed()
}

// set the Elementer to the inner content at position pos and overwrite the current content at that position
func (xyyy *Element) SetAtPosition(pos int, v Elementer) (err error) {
	if pos < 0 || pos > len(xyyy.inner)-1 {
		return fmt.Errorf("position %v out of range", pos)
	}
	if err := xyyy.ensureParentIsSetAndContentIsAllowed(v); err != nil {
		return err
	}
	xyyy.inner[pos] = v
	return
}

// sets the Elementer to the last position of the inner content and overwrites the current content at that position
//
// If you want to append to the inner content, use Add() instead
func (xyyy *Element) SetBottom(v Elementer) (err error) {
	if err := xyyy.ensureParentIsSetAndContentIsAllowed(v); err != nil {
		return err
	}
	xyyy.inner[len(xyyy.inner)-1] = v
	return
}

// returns the position of the Element in the inner content. if it could not be found, the last parameter is false
func (xyyy *Element) PositionOf(v *Element) (pos int, found bool) {
	m := &PositionMatcher{Element: v}
	_ = xyyy.Any(m)
	pos = m.Pos
	found = m.Found
	return
}

// adds Elementer at the position before the Element in the inner content
// the following elements are moved down
func (xyyy *Element) AddBefore(v *Element, nu Elementer) (err error) {
	pos, found := xyyy.PositionOf(v)
	if !found {
		return fmt.Errorf("could not find %#v", v)
	}
	if err := xyyy.ensureParentIsSetAndContentIsAllowed(nu); err != nil {
		return err
	}
	return xyyy.AddAtPosition(pos, nu)
}

// adds Elementer at the position before the Element in the inner content
// the following elements are moved down
func (xyyy *Element) AddAfter(v *Element, nu Elementer) (err error) {
	pos, found := xyyy.PositionOf(v)
	if !found {
		return fmt.Errorf("could not find %#v", v)
	}
	if err := xyyy.ensureParentIsSetAndContentIsAllowed(nu); err != nil {
		return err
	}
	if pos > len(xyyy.inner)-2 {
		return xyyy.Add(nu)
	} else {
		return xyyy.AddAtPosition(pos+1, nu)
	}
}

// adds new inner content or properties based on Stringer objects and returns an error if changes could not be applied
//
// the following types are handled in a special way:
//
//   - Comment: sets the comment
//   - Style: set a single style
//   - Styles: sets multiple styles
//   - Attr: set a single attribute   // do not set id or class via Attr(s) directly, use Id() and Class() instead
//   - Attrs: sets multiple attribute
//   - Class: adds a class
//   - Id: sets the id
//   - *Css: applies the css, see ApplyCss()
//
// the following types are added to  the inner content:
//
//   - Text: ís escaped if the WithoutEscaping flag isn't set
//   - Html: is never escaped
//
// If the Stringer can be casted to an Elementer (as Element can), it is added to the inner content as well
// otherwise it is handled like Text(), that means any type implementing Stringer can be added as (escaped) text
func (xyyy *Element) Add(objects ...interface{}) (err error) {
	for _, o := range objects {
		switch v := o.(type) {
		case templ.Position:
			e := xyyy.Add(string(places.DefaultStartDel) + v.Name() + string(places.DefaultEndDel))
			if e != nil {
				return e
			}
			continue
		case *CompiledTemplate:
			e := xyyy.Add(string(places.DefaultStartDel) + v.Name() + string(places.DefaultEndDel))
			if e != nil {
				return e
			}
			continue
		case Placeholder:
			var e error
			switch tp := v.Type().(type) {
			case Comment:
				e = xyyy.Add(Comment(v.String()))
			case Id:
				e = xyyy.Add(Id(v.String()))
			case Class:
				e = xyyy.Add(Class(v.String()))
			case Html:
				e = xyyy.Add(Html(v.String()))
			case Text:
				e = xyyy.Add(Text(v.String()))
			case SingleAttr:
				e = xyyy.Add(SingleAttr{tp.Key, v.String()})
			case Tag:
				xyyy.tag = Tag(v.String())
			case Style:
				e = xyyy.Add(Style{tp.Property, v.String()})
			default:
				e = fmt.Errorf("%#v (%T) unsupported placeholder type", v, v)
			}
			if e != nil {
				return e
			}
			continue
			//xyyy.inner = append(xyyy.inner, Html(v.Key()))
		case string:
			if err := xyyy.ensureContentAddIsAllowed(); err != nil {
				return err
			}
			if !xyyy.Is(WithoutEscaping) {
				v = html.EscapeString(v)
			}
			xyyy.inner = append(xyyy.inner, Text(v))
		case Text:
			if err := xyyy.ensureContentAddIsAllowed(); err != nil {
				return err
			}
			if !xyyy.Is(WithoutEscaping) {
				v = Text(html.EscapeString(string(v)))
			}
			xyyy.inner = append(xyyy.inner, v)
		case Html:
			if err := xyyy.ensureContentAddIsAllowed(); err != nil {
				return err
			}
			xyyy.inner = append(xyyy.inner, v)
		case Comment:
			xyyy.Comment = v

		case SingleAttr:
			xyyy.SetAttribute(v.Key, v.Value)

		case Attrs:
			for _, atr := range v {
				xyyy.SetAttribute(atr.Key, atr.Value)
			}
		case classes:
			for _, cl := range v {
				if err := xyyy.AddClass(cl); err != nil {
					return err
				}
			}
		case Class:
			if err := xyyy.AddClass(v); err != nil {
				return err
			}
		case Id:
			if err := xyyy.SetId(v); err != nil {
				return err
			}
		case Style:
			xyyy.addStyle(v)
			/*
				case styles:
					xyyy.addStyle(v)
			*/
			/*
				case *Css:
					if err := xyyy.ApplyCss(v); err != nil {
						return err
					}
			*/
		case []Style:
			xyyy.addStyle(v)
		case Stringer:
			if err := xyyy.ensureContentAddIsAllowed(); err != nil {
				return err
			}
			stringer := v
			/*
				stringer, ok := v.(Stringer)
				if !ok {
					return fmt.Errorf("%#v  is no Stringer", v)
				}
			*/
			if err := xyyy.ensureParentIsSetAndContentIsAllowed(stringer); err == nil {
				// no error: is an Elementer
				xyyy.inner = append(xyyy.inner, stringer)
			} else {
				// handle it like untyped string
				s := Text(stringer.String())

				if !xyyy.Is(WithoutEscaping) {
					s = Text(html.EscapeString(stringer.String()))
				}
				xyyy.inner = append(xyyy.inner, s)
			}
		default:
			return fmt.Errorf("%#v  is no Stringer", v)
		}
	}
	return
}

// clears the inner elements and strings
func (xyyy *Element) Clear() {
	xyyy.inner = []Stringer{}
}

func (xyyy *Element) Selecter(other ...Selecter) Selecter {
	return Selector(SelectorString(xyyy.Selector()), other...)
}

/*
func (xyyy *Element) Placeholder() Placeholder {
	return Html(xyyy.String()).Placeholder()
}
*/

func (xyyy *Element) Selector() string {
	sele := []string{xyyy.tag.Selector()}
	if xyyy.id != Id("") {
		sele = append(sele, xyyy.id.Selector())
	}
	for _, c := range xyyy.classes {
		sele = append(sele, c.Selector())
	}
	return strings.Join(sele, "")
}

func (xyyy *Element) AsTemplate() *Template {
	return NewTemplate(xyyy)
}

func (xyyy *Element) Compile(name string) *CompiledTemplate {
	return xyyy.AsTemplate().MustCompile(name)
}

func (xyyy *Element) Placeholder(name string) *CompiledTemplate {
	return xyyy.AsTemplate().MustCompile(name)
}

// clears the inner object array
// and then calles Add() method to add content
//
// see Add() method for more details
func (xyyy *Element) SetContent(objects ...interface{}) (err error) {
	xyyy.inner = []Stringer{}
	return xyyy.Add(objects...)
}

// use this func to set the id of the Element,
// do not set it via Attr directly
// returns error if IdForbidden flag is set
func (xyyy *Element) SetId(id Id) (err error) {
	if xyyy.Is(IdForbidden) {
		return fmt.Errorf("id not allowed for tag %s", xyyy.tag)
	}
	xyyy.id = id
	return
}

func (xyyy *Element) classAttrString(classes []Class) (s string) {
	//s = ""
	var buffer bytes.Buffer
	for _, cl := range classes {
		//s += cl.String() + " "
		buffer.WriteString(cl.String() + " ")
	}
	return buffer.String()
}

func (xyyy *Element) classPath(classes []Class) (s string) {
	//s = ""
	var buffer bytes.Buffer
	for _, cl := range classes {
		//s += "." + cl.String()
		buffer.WriteString("." + cl.String())
	}
	return buffer.String()
}

func (xyyy *Element) styleAttrString(styles map[string]string) (s string) {
	var buffer bytes.Buffer
	//s = ""
	for k, v := range styles {
		//s += Style{k, v}.String()
		buffer.WriteString(Style{k, v}.String())
	}
	return buffer.String()
	//return
}

func (xyyy *Element) HasClass(class Class) bool {
	for _, c := range xyyy.classes {
		if c == class {
			return true
		}
	}
	return false
}

func (xyyy *Element) RemoveClass(class Class) {
	for i, c := range xyyy.classes {
		if c == class {
			// remove an element from a slice, according to
			// https://groups.google.com/forum/?fromgroups=#!topic/golang-nuts/lYz8ftASMQ0
			copy(xyyy.classes[i:], xyyy.classes[i+1:])
			xyyy.classes = xyyy.classes[:len(xyyy.classes)-1]
		}
	}
}

// use this func to set the classes of the Element
// do not set them via Attr directly
func (xyyy *Element) SetClass(classes ...Class) {
	xyyy.classes = []Class{}
	xyyy.AddClass(classes...)
}

// use this func to add the classes of the tag,
// do not set it via Attr directly
func (xyyy *Element) AddClass(classes ...Class) (err error) {
	if xyyy.Is(ClassForbidden) {
		return fmt.Errorf("class not allowed for tag %s", xyyy.tag)
	}
	for _, cl := range classes {
		xyyy.classes = append(xyyy.classes, cl)
	}
	return
}

// use this method to get the classes since they won't show up in attributes
func (xyyy *Element) Classes() (c []Class) {
	return xyyy.classes
}

// use this method to get the id since it won't show up in attributes
func (xyyy *Element) Id() Id { return xyyy.id }

// returns the html with inner content (and the own tags if WithoutDecoration is not set)
func (xyyy *Element) String() (res string) {
	if xyyy.Is(WithoutDecoration) {
		return xyyy.InnerHtml()
	}

	commentpre := ""
	commentpost := ""
	if xyyy.Comment != "" {
		commentpre = fmt.Sprintf("<!-- Begin: %s -->", xyyy.Comment)
		commentpost = fmt.Sprintf("<!-- End: %s -->", xyyy.Comment)
	}
	if xyyy.Is(SelfClosing) {
		res = fmt.Sprintf("%s<%s%s />%s", commentpre, string(xyyy.tag), xyyy.AttrsString(), commentpost)
	} else {
		str := "%s<%s%s>%s</%s>%s"
		//if !xyyy.Is(Inline) {
		//	str = "\n%s<%s%s>%s</%s>%s\n"
		//}
		res = fmt.Sprintf(str, commentpre, string(xyyy.tag), xyyy.AttrsString(), xyyy.InnerHtml(), string(xyyy.tag), commentpost)
	}
	return
}

// prepare the id attribute for output
func (xyyy *Element) AttrsString() (res string) {
	var buffer bytes.Buffer
	//res = ""
	if !xyyy.Is(IdForbidden) && xyyy.id != "" {
		//res += SingleAttr{"id", string(xyyy.id)}.String()
		buffer.WriteString(SingleAttr{"id", string(xyyy.id)}.String())
	}
	if !xyyy.Is(ClassForbidden) && len(xyyy.classes) > 0 {
		//res += SingleAttr{"class", xyyy.classAttrString(xyyy.classes)}.String()
		buffer.WriteString(SingleAttr{"class", xyyy.classAttrString(xyyy.classes)}.String())
	}
	if !xyyy.Is(Invisible) && len(xyyy.style) > 0 {
		//res += SingleAttr{"style", xyyy.styleAttrString(xyyy.style)}.String()
		buffer.WriteString(SingleAttr{"style", xyyy.styleAttrString(xyyy.style)}.String())
	}

	for k, v := range xyyy.attributes {
		//res += SingleAttr{k, v}.String()
		buffer.WriteString(SingleAttr{k, v}.String())
	}
	return buffer.String()
}

func (xyyy *Element) InnerHtml() (res string) {
	var buffer bytes.Buffer
	// res = ""
	for _, in := range xyyy.inner {
		//res += in.String()
		buffer.WriteString(in.String())
	}
	return buffer.String()
}

// returns only children that are Elements, no Text or Html
func (xyyy *Element) Children() (c []*Element) {
	c = []*Element{}
	if len(xyyy.inner) == 0 {
		return
	}
	for _, in := range xyyy.inner {
		switch t := in.(type) {
		case *Element:
			c = append(c, t)
		}
	}
	return
}

// filter by anything that fullfills the matcher interface,
// e.g. Class, Id, Attr, Attrs, Css, Tag, Style, Styles
// recursive finds all tags from the children
func (xyyy *Element) All(m Matcher) (r []*Element) {
	r = []*Element{}
	if len(xyyy.inner) == 0 {
		return
	}
	for _, in := range xyyy.inner {
		switch t := in.(type) {
		case *Element:
			if m.Matches(t) {
				r = append(r, t)
			}
			innerFound := t.All(m)
			for _, innerT := range innerFound {
				r = append(r, innerT)
			}
		}
	}
	return
}

// filter by anything that fullfills the matcher interface,
// e.g. Class, Id, Attr, Attrs, Css, Tag, Style, Styles
// returns the first tag in the children and the subchildren that matches
func (xyyy *Element) Any(m Matcher) (r *Element) {
	if len(xyyy.inner) == 0 {
		return nil
	}
	for _, in := range xyyy.inner {
		switch t := in.(type) {
		case *Element:
			if m.Matches(t) {
				r = t
				return
			}
			r = t.Any(m)
			if r != nil {
				return
			}
		}
	}
	return nil
}
