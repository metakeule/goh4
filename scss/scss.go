package scss

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/metakeule/goh4"
	"gitlab.com/metakeule/goh4/css"
)

type var_ struct {
	Name_ string
	Value string
}

var Parent = goh4.SelectorString("&")

func Var(name string) var_              { return var_{Name_: name} }
func (xyyy var_) Val(value string) var_ { return var_{xyyy.Name_, value} }
func (xyyy var_) Name() string          { return "$" + xyyy.Name_ }
func (xyyy var_) Css() string           { return xyyy.Style() }
func (xyyy var_) Style() string         { return fmt.Sprintf("%s: %s;", xyyy.Name(), xyyy.Value) }
func (xyyy var_) String() string        { return xyyy.Style() }
func (xyyy var_) Selector() string      { return fmt.Sprintf("#{%s}", xyyy.String()) }

func CompileClasses(classes []goh4.Class, file string) error {
	css := []string{}
	for _, c := range classes {
		name := string(c)
		name = strings.Replace(name, "-", "_", -1)
		css = append(css, Var("class_"+name).Val(c.Selector()).Css())
	}
	return ioutil.WriteFile(file, []byte(strings.Join(css, "\n")), os.FileMode(0644))
}

func CompileIds(ids []goh4.Id, file string) error {
	css := []string{}
	for _, c := range ids {
		name := string(c)
		name = strings.Replace(name, "-", "_", -1)
		css = append(css, Var("id_"+name).Val(c.Selector()).Css())
	}
	return ioutil.WriteFile(file, []byte(strings.Join(css, "\n")), os.FileMode(0644))
}

type param struct {
	Var      var_
	default_ string
}

func Param(name string) param { return param{Var: var_{Name_: name}} }

func (xyyy param) Default(s string) param {
	return param{var_{Name_: xyyy.Var.Name_, Value: xyyy.Var.Value}, s}
}

func (xyyy param) String() (s string) {
	if xyyy.default_ == "" {
		s = fmt.Sprintf("%s", xyyy.Var.Name())
	} else {
		s = fmt.Sprintf("%s: %s", xyyy.Var.Name(), xyyy.default_)
	}
	return
}

func (xyyy param) Name() (s string) { return xyyy.Var.Name() }

type mixin struct {
	*css.RuleStruct
	Name string
	Args []string
}

func Mixin(name string, params ...param) (xyyy *mixin) {
	args := []string{}
	for _, param := range params {
		args = append(args, param.String())
	}
	xyyy = &mixin{&css.RuleStruct{}, name, args}
	return
}

func (xyyy *mixin) String() string {
	xyyy.RuleStruct.Selector = xyyy
	return xyyy.RuleStruct.String()
}

func (xyyy *mixin) argString() (s string) { return strings.Join(xyyy.Args, ", ") }

func (xyyy *mixin) Selector() (s string) {
	if len(xyyy.Args) == 0 {
		s = "@mixin " + xyyy.Name
	} else {
		s = fmt.Sprintf("@mixin %s(%s)", xyyy.Name, xyyy.argString())
	}
	return
}

type call mixin
type include mixin

func Call(fn string, args ...string) (xyyy *call) {
	xyyy = &call{}
	xyyy.Name = fn
	xyyy.Args = args
	return
}

func (xyyy *call) Style() (s string) {
	return xyyy.String() + ";"
}

func (xyyy *call) String() (s string) {
	m := mixin(*xyyy)
	s = fmt.Sprintf("%s(%s)", m.Name, m.argString())
	return
}

func Include(mixin string, args ...string) (xyyy *include) {
	xyyy = &include{}
	xyyy.Name = mixin
	xyyy.Args = args
	return
}

func (xyyy *include) Style() (s string) {
	m := mixin(*xyyy)
	if len(m.Args) == 0 {
		s = fmt.Sprintf("@include %s;", m.Name)
	} else {
		s = fmt.Sprintf("@include %s(%s);", m.Name, m.argString())
	}
	return
}
