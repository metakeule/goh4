package initializr

import (
	ŧ "fmt"
	"io/ioutil"
	ħ "net/http"
	"os"
	"path"
	"regexp"
	"strings"

	. "gitlab.com/metakeule/goh4"
	. "gitlab.com/metakeule/goh4/tag"
)

type Loadable interface {
	Load() string
}

type directText string

func (xyyy directText) Load() string {
	// ŧ.Printf("loading: %#v\n", xyyy)
	return string(xyyy)
}

var isUrl = regexp.MustCompile("https?://")

type file string

// load the file and return as string
func (xyyy file) Load() string {
	if isUrl.MatchString(string(xyyy)) {
		// ŧ.Printf("loading: %#v\n", xyyy)
		resp, ſ := ħ.Get(string(xyyy))
		if ſ != nil {
			panic(ſ.Error())
		}
		defer resp.Body.Close()
		body, ſ := ioutil.ReadAll(resp.Body)
		if ſ != nil {
			panic(ſ.Error())
		}
		return string(body)
	} else {
		// ŧ.Printf("loading: %#v\n", xyyy)
		wd, _ := os.Getwd()
		// fmt.Printf("working dir: %s\n", wd)
		f := path.Join(wd, string(xyyy))
		// fmt.Printf("file: %s\n", f)
		b, ſ := ioutil.ReadFile(f)
		if ſ != nil {
			panic(ſ.Error())
		}
		return string(b)
	}
	return ""
}

type Initializr struct {
	*Template
	pre            string
	post           string
	Head           *Element
	Body           *Element
	Navigation     *Element
	Main           *Element
	CssPath        string
	JsPath         string
	UseCachedFiles bool
	cachedJs       []Loadable
	cachedJsHead   []Loadable
	cachedCss      []Loadable
}

var cachedJsPath = "%s/scripts.js"
var cachedJsHeadPath = "%s/scripts-head.js"
var cachedCssPath = "%s/style.css"

var pre string = `<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->`

var post string = `</html>`

var infoChromeFrame = `<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->`

var infoBootstrap = ` <!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->`

var style string = `
  body {
      padding-top: 60px;
      padding-bottom: 40px;
  }
`
var lf string = "\n"
var charset = META(Attr("charset", "utf-8"))
var chromeFrame = META(Attr("http-equiv", "X-UA-Compatible"), Attr("content", "IE=edge,chrome=1"))
var viewport = META(Attr("name", "viewport"), Attr("content", "width=device-width"))

func (xyyy *Initializr) Compile(name string) (*CompiledTemplate, error) {
	xyyy.Template.Add(Html(post))
	return xyyy.Template.Compile(name)
}

func (xyyy *Initializr) MustCompile(name string) *CompiledTemplate {
	t, e := xyyy.Compile(name)
	if e != nil {
		panic(e.Error())
	}
	return t
}

func (xyyy *Initializr) CachedJsFile() (path string, content string) {
	js := []string{}
	// ŧ.Println("CachedJsFile called")
	// ŧ.Printf("cachedJs: %#v\n", xyyy.cachedJs)
	for _, j := range xyyy.cachedJs {
		// ŧ.Println("cachedjs")
		js = append(js, j.Load())
	}
	path = xyyy.jsPathed(cachedJsPath)
	content = strings.Join(js, "\n")
	return
}

func (xyyy *Initializr) CachedJsHeadFile() (path string, content string) {
	js := []string{}
	// ŧ.Println("CachedJsHeadFile called")
	for _, j := range xyyy.cachedJsHead {
		// ŧ.Println("cachedjsHead")
		js = append(js, j.Load())
	}
	path = xyyy.jsPathed(cachedJsHeadPath)
	content = strings.Join(js, "\n")
	return
}

func (xyyy *Initializr) CachedCssFile() (path string, content string) {
	css := []string{}
	// ŧ.Println("CachedCssFile called")
	for _, j := range xyyy.cachedCss {
		// ŧ.Println("cachedCss")
		css = append(css, j.Load())
	}
	path = xyyy.cssPathed(cachedCssPath)
	content = strings.Join(css, "\n")
	return
}

// expects a string with placeholder %s
func (xyyy *Initializr) jsPathed(s string) string {
	return ŧ.Sprintf(s, xyyy.JsPath)
}

// expects a string with placeholder %s
func (xyyy *Initializr) cssPathed(s string) string {
	return ŧ.Sprintf(s, xyyy.CssPath)
}

func (xyyy *Initializr) AddCssFile(f string) {
	xyyy.cachedCss = append(xyyy.cachedCss, file(f))
	// ŧ.Printf("adding css file %v, cachedcss: %#v\n", f, xyyy.cachedCss)
	if !xyyy.UseCachedFiles {
		xyyy.Head.Add(LINK(Attr("rel", "stylesheet"), Attr("href", f)), lf)
	}
}

func (xyyy *Initializr) AddMetaDescription(descr string) {
	xyyy.Head.Add(META(Attr("name", "description"), Attr("content", descr)), lf)
}

func (xyyy *Initializr) AddStyle(css string) {
	// ŧ.Printf("adding css %v cachedcss: %#v\n", css, xyyy.cachedCss)
	xyyy.cachedCss = append(xyyy.cachedCss, directText(css))
	if !xyyy.UseCachedFiles {
		styleTag := NewElement("style", Invisible)
		styleTag.Add(Html("\n" + css + "\n"))
		xyyy.Head.Add(styleTag)
	}
}

func (xyyy *Initializr) AddScriptFile(f string) {
	// ŧ.Printf("adding js file %v cachedJs: %#v\n", f, xyyy.cachedJs)
	xyyy.cachedJs = append(xyyy.cachedJs, file(f))
	if !xyyy.UseCachedFiles {
		xyyy.Body.Add(SCRIPT(Attr("src", f)))
	}
}

func (xyyy *Initializr) AddScriptFileHead(f string) {
	// ŧ.Printf("adding js file head %v cachedJsHead: %#v\n", f, xyyy.cachedJsHead)
	xyyy.cachedJsHead = append(xyyy.cachedJsHead, file(f))
	if !xyyy.UseCachedFiles {
		xyyy.Head.Add(SCRIPT(Attr("src", f)))
	}
}

func (xyyy *Initializr) AddScriptHead(js string) {
	// ŧ.Printf("adding js head %v cachedJsHead: %#v\n", js, xyyy.cachedJsHead)
	xyyy.cachedJsHead = append(xyyy.cachedJsHead, directText(js))
	if !xyyy.UseCachedFiles {
		xyyy.Head.Add(SCRIPT(Html(js)))
	}
}

func (xyyy *Initializr) AddScript(js string) {
	// ŧ.Printf("adding js %v cachedJs: %#v\n", js, xyyy.cachedJs)
	xyyy.cachedJs = append(xyyy.cachedJs, directText(js))
	if !xyyy.UseCachedFiles {
		xyyy.Body.Add(SCRIPT(Html(js)))
	}
}

func (xyyy *Initializr) AddGoogleAnalytics(account string) {
	xyyy.AddScript(`var _gaq=[['_setAccount','` + account + `'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));`)
}

func (xyyy *Initializr) NewContainter() *Element {
	return DIV(Class("container"))
}

func (xyyy *Initializr) SetupHead() {
	if xyyy.UseCachedFiles {
		xyyy.Head.Add(LINK(Attr("rel", "stylesheet"), Attr("href", xyyy.cssPathed(cachedCssPath))), lf)
		xyyy.Head.Add(SCRIPT(Attr("src", xyyy.jsPathed(cachedJsHeadPath))))
	} else {
		xyyy.AddCssFile(xyyy.cssPathed("%s/bootstrap.min.css"))
		xyyy.AddStyle(style)
		xyyy.AddCssFile(xyyy.cssPathed("%s/bootstrap-responsive.min.css"))
		//xyyy.AddCssFile(xyyy.cssPathed("%s/main.css"))
		xyyy.Head.Add(SCRIPT(Attr("src", xyyy.jsPathed("%s/modernizr-2.6.2-respond-1.1.0.min.js"))))
	}
}

func (xyyy *Initializr) SetupBody() {
	xyyy.Body.Add(Html(infoChromeFrame))
	xyyy.Body.Add(Html(infoBootstrap))
	xyyy.Navigation = xyyy.NewContainter()
	navBar := DIV(
		Class("navbar"),
		Class("navbar-inverse"),
		Class("navbar-fixed-top"),
		DIV(
			Class("navbar-inner"),
			xyyy.Navigation))
	xyyy.Body.Add(navBar)

	xyyy.Main = xyyy.NewContainter()
	xyyy.Body.Add(xyyy.Main)
	if xyyy.UseCachedFiles {
		xyyy.Body.Add(SCRIPT(Attr("src", xyyy.jsPathed(cachedJsPath))))
	} else {
		//xyyy.AddScriptFileHead("http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js")
		//xyyy.AddScriptHead(xyyy.jsPathed(`window.jQuery || document.write('<script src="%s/jquery-1.8.3.min.js"><\/script>')`))
		xyyy.AddScriptFileHead(xyyy.jsPathed("%s/jquery-1.8.3.min.js"))
		//xyyy.AddScriptFile(xyyy.jsPathed(`%s/jquery-1.8.3.min.js`))
		//xyyy.AddScriptFile(xyyy.jsPathed("%s/bootstrap.min.js"))
		xyyy.AddScriptFile(xyyy.jsPathed("%s/bootstrap.js"))
		xyyy.AddScriptFile(xyyy.jsPathed("%s/main.js"))
	}
}

func Layout() (layout *Initializr) {
	layout = &Initializr{
		Template:     &Template{Element: Doc(Html(pre))},
		pre:          pre,
		post:         post,
		Head:         HEAD(lf, charset, lf, chromeFrame, lf, viewport, lf),
		Body:         BODY(),
		CssPath:      "/css",
		JsPath:       "/js",
		cachedJs:     []Loadable{},
		cachedJsHead: []Loadable{},
		cachedCss:    []Loadable{},
	}
	//layout.Template.Tag = layout.Body
	layout.Template.Add(layout.Head, layout.Body)
	return
}
