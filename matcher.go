package goh4

import (
	"strings"
)

// something that matches an Element
type Matcher interface {
	Matches(*Element) bool
}

type or []Matcher

// matches if any of the Matchers matches
func (xyyy or) Matches(e *Element) bool {
	for _, m := range xyyy {
		if m.Matches(e) {
			return true
		}
	}
	return false
}

func Or(m ...Matcher) or {
	return or(m)
}

type and []Matcher

// matches if all of the Matchers matches
func (xyyy and) Matches(e *Element) bool {
	for _, m := range xyyy {
		if !m.Matches(e) {
			return false
		}
	}
	return true
}

func And(m ...Matcher) and {
	return and(m)
}

type not struct{ Matcher }

// matches if inner matcher does not match
func (xyyy *not) Matches(e *Element) bool { return !xyyy.Matcher.Matches(e) }

func Not(m Matcher) *not { return &not{m} }

type PositionMatcher struct {
	Element *Element
	Pos     int
	Found   bool
}

func (xyyy *PositionMatcher) Matches(e *Element) (f bool) {
	// no recursive findings
	if e.Parent() != xyyy.Element.Parent() {
		return
	}
	if xyyy.Element == e {
		xyyy.Found = true
		return true
	}
	if !xyyy.Found {
		xyyy.Pos += 1
	}
	return
}

type FieldMatcher int

func (xyyy FieldMatcher) Matches(t *Element) (m bool) { return t.Is(FormField) }

func (xyyy Class) Matches(t *Element) bool {
	for _, c := range t.classes {
		if c == xyyy {
			return true
		}
	}
	return false
}

// if Css has a Context, matching always fails
/*
func (xyyy *Css) Matches(t *Element) (m bool) {
	if xyyy.Context != "" {
		// if Css has a Context, matching always fails
		return false
	}
	if xyyy.class != "" {
		if !t.HasClass(xyyy.class) {
			return false
		}
	}

	if len(xyyy.Tags) > 0 {
		for _, tt := range xyyy.Tags {
			if xyyy.matchTag(t, Tag(tt)) {
				return true
			}
		}
	} else {
		return true
	}

	return
}
*/

func (xyyy Id) Matches(t *Element) bool {
	if string(t.id) == string(xyyy) {
		return true
	}
	return false
}

// matching an html string, ignoring whitespace
func (xyyy Html) Matches(t *Element) bool {
	inner := removeWhiteSpace(t.InnerHtml())
	me := removeWhiteSpace(xyyy.String())
	if inner == me {
		return true
	}
	return false
}

func (xyyy Tag) Matches(t *Element) bool {
	return xyyy == t.tag
}

func (xyyy Style) Matches(t *Element) bool {
	return t.style[xyyy.Property] == xyyy.Value
}

/*
func (xyyy styles) Matches(t *Element) (m bool) {
	m = true
	for _, styl := range xyyy {
		if !styl.Matches(t) {
			m = false
		}
	}
	return
}
*/

func (xyyy SingleAttr) Matches(t *Element) bool {
	if xyyy.Key == "id" {
		return Id(xyyy.Value).Matches(t)
	}
	if xyyy.Key == "class" {
		return Class(xyyy.Value).Matches(t)
	}
	if xyyy.Key == "style" {
		styles := strings.Split(xyyy.Value, ";")
		m := true
		for _, st := range styles {
			a := strings.Split(st, ":")
			styl := Style{a[0], a[1]}
			if !styl.Matches(t) {
				m = false
			}
		}
		return m
	}
	if t.attributes[xyyy.Key] == xyyy.Value {
		return true
	}
	return false
}

func (xyyy Attrs) Matches(t *Element) (m bool) {
	m = true
	for _, attr := range xyyy {
		if !attr.Matches(t) {
			m = false
		}
	}
	return
}
